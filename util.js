const slackbot = require('botkit').slackbot;
const request = require('request');

const controller = slackbot({
    debug: false,
    log: true
});

const botScope = [
    'direct_message',
    'direct_mention',
    'mention'
];

const bot = controller.spawn({
    token: 'xoxb-74780488772-1789298229541-eLqounxwoE4HhOMsAjLHg6cT'
}).startRTM();

function get_jira_ticket(user) {

    return new Promise((resolve, reject) => {
        request.get({
            headers: {'authorization': 'Basic dGFleWFuZ2xpbUBtaW5kc2xhYi5haTp4b2RpZDE1NDEhIUA=', 'Content-Type': 'application/json'},
            url: 'https://pms.maum.ai/jira/rest/api/2/search?jql=assignee= \"' + user.user_email + '\" and status not in (resolved, closed, Done)',
            json: true
        }, function(error, response, body){
            if (response.statusCode != 200) {
                console.log(error);
            } else {
                console.log(body);
                let idAndSummary = new Map();
                let projectSet = new Set();
                const issues = body.issues;
                issues.forEach(it => {
                    idAndSummary.set(it.id, {
                        project: it.fields.project.name,
                        summary: it.fields.summary
                    });
                    projectSet.add(it.fields.project.name);
                });
                console.log(idAndSummary);
    
                const reply_attachments = {
                    'channel': user.user_slack_id,
                    'text': '<@'+user.user_slack_id+'>님 현재 완료되지 않은 티켓 목록은 다음과 같습니다.',
                    'attachments': [
                    ]
                };
                
                projectSet.forEach(setVal => {
                    let text_message = "";
                    idAndSummary.forEach((item, key) => {
                        if (setVal == item.project) {
                            text_message += "<https://pms.maum.ai/jira/issues/?jql=id=" + key  + "|" + item.summary + ">" + "\n";
                        }
                    });
                    reply_attachments.attachments.push({
                        title: setVal,
                        text: text_message
                    });
                });
                resolve(reply_attachments);
            }
        });
    });

}

function response_attachments(text, attachmentsTitle, attachmentsText, color) {
    return {
        'text': text,
        'attachments': [
            {
                'mrkdwn_in': ["text"],
                'title': attachmentsTitle,
                'text': attachmentsText,
                'color': color
            }
        ]
    }
}

exports.controller = controller;
exports.botScope = botScope;
exports.bot = bot;
exports.get_jira_ticket = get_jira_ticket;
exports.response_attachments = response_attachments;
