FROM node:12

WORKDIR /app

ENV TZ=Asia/Seoul
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

COPY package.json ./

COPY . .

RUN npm install

CMD [ "node", "bot.js" ]