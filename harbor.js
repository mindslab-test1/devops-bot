const { controller, botScope, response_attachments } = require('./util');
const request = require('request');

function harbor_controller() {

    controller.hears(['harbor:getId'], botScope, (bot, message) => {
        const projectName = message.text.replace(/\s/g, '').split(",")[1];
    
        request.get({
            headers: {'authorization': 'Basic YWRtaW46SGFyYm9yMTIzNDU=', 'Content-Type': 'application/json'},
            url: 'https://aics-stg.maum.ai:8443/api/v2.0/projects?name=' + projectName,
            json: true
        }, function(error, response, body) {
            if (response.statusCode != 200) {
                bot.reply(message, response_attachments('<@'+message.user+'>님 요청하신 조회가 실패하였습니다.',
                'Harbor Get ProjectId', 'error) " + body.errors[0].message', '#FF0000'));
            } else {
                if (body.length != 0) {
                    const project_id = body[0].project_id;
                    bot.reply(message, response_attachments('<@'+message.user+'>님 요청하신 조회가 완료되었습니다.',
                    'Harbor Get ProjectId', "`project_id`: " + project_id, '#7CD197')); 
                } else {
                    bot.reply(message, response_attachments('<@'+message.user+'>님 조회하신 프로젝트가 존재하지 않습니다.',
                    'Harbor Get ProjectId', 'error) Project Not Found', '#FF0000'));
                }
            }
        });
    });

    controller.hears(['harbor:create'], botScope, (bot, message) => {
        const userInput = message.text.replace(/\s/g, '').split(',');
    
        bot.api.users.info({user: message.user}, function(err, info) {
            const param = {
                username: userInput[1], // id
                password: userInput[2], // password
                email: info.user.profile.email, // email
                realname: info.user.profile.display_name_normalized, // name
                comment: userInput[3] // team
            };
    
            request.post({
                headers: {'authorization': 'Basic YWRtaW46SGFyYm9yMTIzNDU=', 'Content-Type': 'application/json'},
                url: 'https://aics-stg.maum.ai:8443/api/v2.0/users',
                body: param,
                json: true
            }, function(error, response, body) {
                if(response.statusCode === 201) {
                    bot.reply(message, response_attachments('<@'+message.user+'>님 요청하신 Harbor Account가 생성되었습니다.',
                    'Account Create Success', '`ID`: ' + param.username + ', `email`: ' + param.email + ', `team`: ' + param.comment, '#7CD197'));
                } else {
                    bot.reply(message, response_attachments('<@'+message.user+'>님 요청하신 Harbor Account 생성에 실패하였습니다.',
                    'Account Create Failed', "error) " + body.errors[0].message, '#FF0000'));
                }
            });
        });
    });

    controller.hears('harbor:role', botScope, (bot, message) => {
        // 1 = project_id, 2 = role_id, 3 = user_id
        const userInput = message.text.replace(/\s/g, '').split(',');
        const param = {
            "role_id": userInput[2] *= 1,
            "member_user": {
                "username": userInput[3]
            }
        };
    
        request.post({
            headers: {'authorization': 'Basic YWRtaW46SGFyYm9yMTIzNDU=', 'Content-Type': 'application/json'},
            url: "https://aics-stg.maum.ai:8443/api/v2.0/projects/"+ userInput[1] +"/members",
            body: param,
            json: true
        }, function(error, response, body) {
            if(response.statusCode === 201) {
                bot.reply(message, response_attachments('<@'+message.user+'>님 요청하신 Harbor Add Member to Project 설정이 완료되었습니다.', 
                'Project ADD Success', '`ID`: ' + param.member_user.username + ', `project_id`: ' + userInput[1], '#7CD197'));
            } else {
                bot.reply(message, response_attachments('<@'+message.user+'>님 요청하신 Harbor Add Member to Project 설정에 실패하였습니다.', 
                'Project ADD Failed', "error) " + body.errors[0].message, '#FF0000'));
            }
        });
    });    
}

exports.harbor_controller = harbor_controller;