import { slackbot } from 'botkit';
import request from 'request';

const controller = slackbot({
    debug: false,
    log: true
});

const botScope = [
    'direct_message',
	'direct_mention',
	'mention'
]

controller.hears(['command'], botScope, (bot, message) => {
    const reply_attachments = {
        'text': '안녕하세요 <@'+message.user+'>님 \n 사용할 수 있는 커맨드는 아래와 같습니다.',
        'attachments': [
            {
                'mrkdwn_in': ["text"],
                'title': '[Harbor] Create Account',
                'text': '`command`: harbor:create,id,password,team',
                'color': '#7CD197'
            },
            {
                'mrkdwn_in': ["text"],
                'title': '[Harbor] add Project Member',
                'text': '`command`: harbor:role,project_id,role_id,harbor_id \n>role_id `1` for projectAdmin, `2` for developer, `3` for guest, `4` for maintainer',
                'color': '#7CD197'
            },
            {
                'mrkdwn_in': ["text"],
                'title': '[Harbor] get Project ID',
                'text': '`command`: harbor:getId,project_name',
                'color': '#7CD197'
            },
            {
                'mrkdwn_in': ["text"],
                'title': 'jira',
                'text': 'find jira ticket status not in (resolved, closed, Done)',
                'color': '#7CD197'
            }
        ]
    };

    bot.reply(message, reply_attachments);
});

controller.hears(['harbor:getId'], botScope, (bot, message) => {
    const projectName = message.text.replace(/\s/g, '').split(",")[1];

    request.get({
        headers: {'authorization': 'Basic YWRtaW46SGFyYm9yMTIzNDU=', 'Content-Type': 'application/json'},
        url: 'https://aics-stg.maum.ai:8443/api/v2.0/projects?name=' + projectName,
        json: true
    }, function(error, response, body) {
        if (response.statusCode != 200) {
            bot.reply(message, {
                'text': '<@'+message.user+'>님 요청하신 조회가 실패하였습니다.',
                'attachments': [
                    {
                        'title': 'Harbor Get ProjectId',
                        'text': "error) " + body.errors[0].message,
                        'color': '#FF0000'
                    }
                ]
            });
        } else {
            if (body.length != 0) {
                const project_id = body[0].project_id;
                bot.reply(message, {
                    'text': '<@'+message.user+'>님 요청하신 조회가 완료되었습니다.',
                    'attachments': [
                        {
                            'mrkdwn_in': ["text"],
                            'title': 'Harbor Get ProjectId',
                            'text': "`project_id`: " + body[0].project_id,
                            'color': '#7CD197'
                        }
                    ]
                }); 
            } else {
                bot.reply(message, {
                    'text': '<@'+message.user+'>님 조회하신 프로젝트가 존재하지 않습니다.',
                    'attachments': [
                        {
                            'title': 'Harbor Get ProjectId',
                            'text': "error) Project Not Found",
                            'color': '#FF0000'
                        }
                    ]
                });
            }
        }
    });
});

controller.hears(['harbor:create'], botScope, (bot, message) => {
    const userInput = message.text.replace(/\s/g, '').split(',');

    bot.api.users.info({user: message.user}, function(err, info) {
        const param = {
            username: userInput[1], // id
            password: userInput[2], // password
            email: info.user.profile.email, // email
            realname: info.user.profile.display_name_normalized, // name
            comment: userInput[3] // team
        };

        request.post({
            headers: {'authorization': 'Basic YWRtaW46SGFyYm9yMTIzNDU=', 'Content-Type': 'application/json'},
            url: 'https://aics-stg.maum.ai:8443/api/v2.0/users',
            body: param,
            json: true
        }, function(error, response, body) {
            if(response.statusCode === 201) {
                bot.reply(message, {
                    'text': '<@'+message.user+'>님 요청하신 Harbor Account가 생성되었습니다.',
                    'attachments': [
                        {   
                            'mrkdwn_in': ["text"],
                            'title': 'Account Create Success',
                            'text': '`ID`: ' + param.username + ', `email`: ' + param.email + ', `team`: ' + param.comment,
                            'color': '#7CD197'
                        }
                    ]
                });
            } else {
                bot.reply(message, {
                    'text': '<@'+message.user+'>님 요청하신 Harbor Account 생성에 실패하였습니다.',
                    'attachments': [
                        {
                            'mrkdwn_in': ["text"],
                            'title': 'Account Create Failed',
                            'text': "error) " + body.errors[0].message,
                            'color': '#FF0000'
                        }
                    ]
                });
            }
        });
    });
});

controller.hears('harbor:role', botScope, (bot, message) => {
    // 1 = project_id, 2 = role_id, 3 = user_id
    const userInput = message.text.replace(/\s/g, '').split(',');
    const param = {
        "role_id": userInput[2] *= 1,
        "member_user": {
            "username": userInput[3]
        }
    };

    request.post({
        headers: {'authorization': 'Basic YWRtaW46SGFyYm9yMTIzNDU=', 'Content-Type': 'application/json'},
        url: "https://aics-stg.maum.ai:8443/api/v2.0/projects/"+ userInput[1] +"/members",
        body: param,
        json: true
    }, function(error, response, body) {
        if(response.statusCode === 201) {
            bot.reply(message, {
                'text': '<@'+message.user+'>님 요청하신 Harbor Add Member to Project 설정이 완료되었습니다.',
                'attachments': [
                    {   
                        'mrkdwn_in': ["text"],
                        'title': 'Account Create Success',
                        'text': '`ID`: ' + param.member_user.username + ', `project_id`: ' + userInput[1],
                        'color': '#7CD197'
                    }
                ]
            });
        } else {
            bot.reply(message, {
                'text': '<@'+message.user+'>님 요청하신 Harbor Add Member to Project 설정에 실패하였습니다.',
                'attachments': [
                    {
                        'mrkdwn_in': ["text"],
                        'title': 'Account Create Failed',
                        'text': "error) " + body.errors[0].message,
                        'color': '#FF0000'
                    }
                ]
            });
        }
    });
});

controller.hears('죠습니다', botScope, (bot, message) => {
    const reply_msg = {
        "attachments": [
            {
                "title": " ",
                "image_url": "https://media.maum.ai/media/aics/test/image/234b06007ef24c9eb427be02de57666f_slack_img.png"
            }
        ]
    }
    bot.reply(message, reply_msg);
});

controller.hears('jira', botScope, (bot, message) => {
    bot.reply(message, '<@'+message.user+'>님 요청하신 티켓을 조회하겠습니다.');
    bot.api.users.info({user: message.user}, function(err, info){
        //check if it's the right user using info.user.name or info.user.id
        request.get({
            headers: {'authorization': 'Basic dGFleWFuZ2xpbUBtaW5kc2xhYi5haTp4b2RpZDE1NDEhIUA=', 'Content-Type': 'application/json'},
            url: 'https://pms.maum.ai/jira/rest/api/2/search?jql=assignee= \"' + info.user.profile.email + '\" and status not in (resolved, closed, Done)',
            json: true
        }, function(error, response, body){
            if (response.statusCode != 200) {
                bot.reply(message, '<@'+message.user+'>님 다시 요청을 시도해주세요.');
            } else {
                let idAndSummary = new Map();
                let projectSet = new Set();
                const issues = body.issues;
                issues.forEach(it => {
                    idAndSummary.set(it.id, {
                        project: it.fields.project.name,
                        summary: it.fields.summary
                    });
                    projectSet.add(it.fields.project.name);
                });
                console.log(idAndSummary);

                const reply_attachments = {
                    'text': '<@'+message.user+'>님 조회 완료되었습니다 현재 티켓 목록은 다음과 같습니다.',
                    'attachments': [
                    ]
                };
                
                projectSet.forEach(setVal => {
                    let text_message = "";
                    idAndSummary.forEach((item, key) => {
                        if (setVal == item.project) {
                            text_message += "<https://pms.maum.ai/jira/issues/?jql=id=" + key  + "|" + item.summary + ">" + "\n";
                        }
                    });
                    reply_attachments.attachments.push({
                        title: setVal,
                        text: text_message
                    });
                });

                console.log(reply_attachments);
                bot.reply(message, reply_attachments);
            }
        });
    })
});


controller.spawn({
    token: 'xoxb-74780488772-1789298229541-eLqounxwoE4HhOMsAjLHg6cT'
}).startRTM();