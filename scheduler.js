const models = require("./models");
const { controller, botScope, bot, get_jira_ticket, response_attachments } = require('./util');
const scheduler = require('node-schedule');

async function node_scheduler() {

    scheduler.scheduleJob('0 9 * * 1-5', function() {
        models.User.findAll().then(result => {
            result.forEach(user => {
                get_jira_ticket(user).then(response_jira_ticket => bot.say(response_jira_ticket));
            });
        }).catch(error => console.log(error));
    });

    scheduler.scheduleJob('0 12 * * 1-5', function() {
        console.log('abc');
    });

    controller.hears('schedule:del', botScope, (bot, message) => {
    
        const user_slack_id = message.user;

        models.User.findOne({where: {user_slack_id: user_slack_id}})
            .then(user => {
                if(user) {
                    models.User.destroy({where: {user_slack_id: user_slack_id}})
                        .then(result => {
                            bot.reply(message, response_attachments('<@'+message.user+'>님 삭제가 완료되었습니다.', 'jira issue scheduler',
                            "[" + user_slack_id + "]", '#7CD197'));
                        }).catch(error => {
                            console.log(error);
                        });
                } else {
                    bot.reply(message, response_attachments('<@'+message.user+'>님 존재하지 않는 유저입니다.', 'jira issue scheduler',
                    'Not Found User', '#FF0000'));
                } 
        }).catch(error => console.log(error));
    });

    controller.hears('schedule:add', botScope, (bot, message) => {
        bot.api.users.info({user: message.user}, (err, info) => {
            const user_slack_id = message.user;
            const user_email = info.user.profile.email;

            models.User.create({
                user_email: user_email,
                user_slack_id: user_slack_id
            }).then(_ => bot.reply(message, response_attachments('<@'+message.user+'>님 등록이 완료되었습니다.', 
            'jira issue scheduler', "["+message.user+"]", "#7CD197")))
            .catch(error => {
                console.log(error);
                bot.reply(message, response_attachments('<@'+message.user+'>님 등록에 실패하였습니다.', 'jira issue scheduler', "error) ["+error.original.sqlMessage+"]", '#FF0000'));
            });
        });
    });
}

exports.node_scheduler = node_scheduler;