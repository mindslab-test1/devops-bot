const harbor_controller = require('./harbor').harbor_controller;
const { controller, botScope, get_jira_ticket } = require('./util');
const node_scheduler = require('./scheduler').node_scheduler;

controller.hears(['command'], botScope, (bot, message) => {
    const reply_attachments = {
        'text': '안녕하세요 <@'+message.user+'>님 \n 사용할 수 있는 커맨드는 아래와 같습니다.',
        'attachments': [
            {
                'mrkdwn_in': ["text"],
                'title': '[Harbor] Create Account',
                'text': '`command`: harbor:create,id,password,team',
                'color': '#7CD197'
            },
            {
                'mrkdwn_in': ["text"],
                'title': '[Harbor] add Project Member',
                'text': '`command`: harbor:role,project_id,role_id,harbor_id \n>role_id `1` for projectAdmin, `2` for developer, `3` for guest, `4` for maintainer',
                'color': '#7CD197'
            },
            {
                'mrkdwn_in': ["text"],
                'title': '[Harbor] get Project ID',
                'text': '`command`: harbor:getId,project_name',
                'color': '#7CD197'
            },
            {
                'mrkdwn_in': ["text"],
                'title': 'jira',
                'text': 'find jira ticket status not in (resolved, closed, Done)',
                'color': '#7CD197'
            },
            {
                'mrkdwn_in': ["text"],
                'title': 'schedule:add',
                'text': 'insert into jira_scheduler \n `command`: schedule:add',
                'color': '#7CD197'
            },
            {
                'mrkdwn_in': ["text"],
                'title': 'schedule:del',
                'text': 'delete from jira_scheduler \n `command`: schedule:del',
                'color': '#7CD197'
            }
        ]
    };

    bot.reply(message, reply_attachments);
});

controller.hears('jira', botScope, (bot, message) => {
    bot.reply(message, '<@'+message.user+'>님 요청하신 티켓을 조회하겠습니다.');
    bot.api.users.info({user: message.user}, (err, info) => {
        get_jira_ticket({
            user_email: info.user.profile.email,
            user_slack_id: message.user
        }).then(response_jira_ticket => bot.reply(message, response_jira_ticket));
    });
});

harbor_controller();

node_scheduler();